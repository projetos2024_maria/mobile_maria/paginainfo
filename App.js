import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import TaskList from "./src/taskList";
import TaskDetails from "./src/taskDetails";
import { StyleSheet } from "react-native";

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName='ListaDeTarefas'>
        <Stack.Screen name='ListaDeTarefas' component={TaskList} />
        <Stack.Screen
          name='DetalhesDasTarefas'
          component={TaskDetails}
          options={{ title: 'Informações das tarefas' }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
