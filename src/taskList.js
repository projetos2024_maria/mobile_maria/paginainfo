import React from "react";
import { View, Text, TouchableOpacity, FlatList, StyleSheet } from "react-native";

const TaskList = ({ navigation }) => {
  const tasks = [
    {
      id: 1,
      title: "hidratar o cabelo",
      date: "2024-02-27",
      time: "13:00",
      address: "Casa",
    },
    {
      id: 2,
      title: "ir ao supermercado",
      date: "2024-02-27",
      time: "15:30",
      address: "Supermercado Central",
    },
    {
      id: 3,
      title: "fazer exercícios físicos",
      date: "2024-02-27",
      time: "17:00",
      address: "Academia Fitness Plus",
    },
  ];
  const taskPress=()=>{
        navigation.navigate('DetalhesDasTarefas',{task});
  }

  return (
    <View>
      <FlatList data={tasks}
      keyExtractor={(item)=> item.id.toString}
      renderItem={({item})=>(
        <TouchableOpacity onPress={() => taskPress(item)}>
            <Text>{item.title}</Text>
        </TouchableOpacity>
      )}/>
    </View>
  );
};
export default TaskList;
